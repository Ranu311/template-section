import React from "react";
import { ThemeProvider } from "@mui/material";
import { ApolloProvider } from "@apollo/client";
import MainRouter from "./MainRouter";
import client from "./contexts/apollo";
import THEME from "./constants/theme";

const App = () => {
	return (
		<ThemeProvider theme={THEME}>
			<ApolloProvider client={client}>
				<MainRouter />
			</ApolloProvider>
		</ThemeProvider>
	);
};

export default App;
