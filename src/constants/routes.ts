enum ROUTES {
    HOME = "/",
    PROFILE = "/profile",
    SETTINGS = "/settings",
    BILLINGS = "/billings",
    DRIVE_TRAILS='/drive-trials',
    ANALYTICS='/analytics',
    USER='/user',
    NOTIFICATION='/notification',
    CHATBOT='/chatbot',
    CREATE_BOT='/create',
    TEMPLATES='/templates'
}

export default ROUTES;
