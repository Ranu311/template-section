import React, { useState } from "react";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import ROUTES from "./constants/routes";
import Users from "pages/Users";
import BotHome from "pages/BotHome";
import Chatbot from "pages/Chatbot";
import CreateBot from "pages/CreateBot";
import Templates from "pages/Templates";
const MainRouter = () => {
    const [drawerOpen, setDrawerOpen] = useState(true);

    return (
        <Router>
            <Routes>
                <Route path={ROUTES.HOME} element={<BotHome drawerOpen={drawerOpen} setDrawerOpen={setDrawerOpen} />} />
                <Route path={ROUTES.USER} element={<Users drawerOpen={drawerOpen} setDrawerOpen={setDrawerOpen} />} />
                <Route path={ROUTES.CHATBOT} element={<Chatbot />} />
                <Route path={ROUTES.CREATE_BOT} element={<CreateBot drawerOpen={drawerOpen} setDrawerOpen={setDrawerOpen} />} />
                <Route path={ROUTES.TEMPLATES} element={<Templates drawerOpen={drawerOpen} setDrawerOpen={setDrawerOpen}/>} />
            </Routes>
        </Router>
    );
};

export default MainRouter;
