import { Fragment } from "react";
import { NavLink } from "react-router-dom";
import List from "@mui/material/List";
import Grid from "@mui/material/Grid";
import ROUTES from "constants/routes";
const BOT_ROUTE = {
    route: ROUTES.HOME,
    title: "Bot",
    icon: <img src="assets/images/Bot(2).png" alt="bot" className="sidebar__icon " />,
};
const CREATE_BOT_ROUTE = {
    route: ROUTES.CREATE_BOT,
    title: "Create Bot",
    icon: <img src="assets/images/Bot(2).png" alt="bot" className="sidebar__icon" />,
};
const DRIVE_TRIAL_ROUTE = {
    route: ROUTES.DRIVE_TRAILS,
    title: "Train Bot",
    icon: <img src="assets/images/DriveTrails.png" alt="drive trials" className="sidebar__icon" />,
};
const ANALYTICS_ROUTE = {
    route: ROUTES.ANALYTICS,
    title: "Analytics",
    icon: <img src="assets/images/analytics.png" alt="analytics" className="sidebar__icon" />,
};
const USER_ROUTE = {
    route: ROUTES.USER,
    title: "Users",
    icon: <img src="assets/images/Profile.png" alt="user" className="sidebar__icon" />,
};
const NOTIFICATION_ROUTE = {
    route: ROUTES.NOTIFICATION,
    title: "Notification",
    icon: <img src="assets/images/Notification.png" alt="notification" className="sidebar__icon" />,
};
const TEMPLATES_ROUTE = {
    route: ROUTES.TEMPLATES,
    title: "Templates",
    icon: <img src="assets/images/Templates.png" alt="templates" className="sidebar__icon" />,
};
const SIDE_BAR_ROUTES = [CREATE_BOT_ROUTE, BOT_ROUTE, DRIVE_TRIAL_ROUTE, ANALYTICS_ROUTE, USER_ROUTE, NOTIFICATION_ROUTE,TEMPLATES_ROUTE];
interface Props {
    drawerOpen: boolean;
}
const Sidebar: React.FC<Props> = ({ drawerOpen }) => {
    return (
        <Fragment>
            <List id="sidebar-container">
                <Grid container direction="column" alignItems="center">
                    {SIDE_BAR_ROUTES.map(({ route, icon, title }) => (
                        <NavLink
                            to={route}
                            key={route}
                            className={window.location.pathname === route ? "link1" : "link1"}
                        >
                            <Grid container alignItems="center">
                                {icon}
                                {drawerOpen && <span className="nav__title">{title}</span>}
                            </Grid>
                        </NavLink>
                    ))}
                </Grid>
            </List>
        </Fragment>
    );
};

export default Sidebar;
