import React, { Fragment } from "react";
import { Drawer, Grid, Typography } from "@mui/material";
import Sidebar from "./Sidebar";
import "../styles/navbar.scss";
// import KeyboardArrowRightIcon from "@mui/icons-material/KeyboardArrowRight";
// import KeyboardArrowLeftIcon from "@mui/icons-material/KeyboardArrowLeft";
const drawerLong = 220;
const drawerShort = 74;
interface Props {
    drawerOpen: boolean;
    setDrawerOpen: React.Dispatch<React.SetStateAction<boolean>>;
}
const Navbar: React.FC<Props> = ({ drawerOpen, setDrawerOpen }) => {
    return (
        <Fragment>
            <Grid container direction="row" justifyContent="space-between" className="navbar__container" alignItems="center">
                <Grid item xs={6} justifyContent="flex-start">
                    <Typography component="h5" variant="h5" className="logo">
                        LOGO
                        <span className="vl"></span>
                        <span className="route">Bots/ Customer Service Bot</span>
                    </Typography>
                </Grid>
                <Grid item xs={6} container justifyContent="flex-end">
                    <Grid item container direction="row" justifyContent="flex-end" alignItems="center">
                        <img src="assets/images/Avtar.png" alt="Avtar" className="avtar" />
                        <Grid item className="user__detail">
                            <Typography component="p" variant="h6" className="username">
                                Udit Mathur
                            </Typography>
                            <Typography component="p" variant="h6" className="usertype">
                                Basic
                            </Typography>
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>
            {drawerOpen ? (
                <img src="assets/images/Left_Arrow.png" onClick={() => setDrawerOpen(false)} className="arrow right" alt="Arrow left" />
            ) : (
                <img src="assets/images/Right_Arrow.png" onClick={() => setDrawerOpen(true)} className="arrow left" alt="Arrow Right" />
            )}
            <Drawer
                sx={{
                    width: drawerOpen ? drawerLong : drawerShort,
                    flexShrink: 0,
                    [`& .MuiDrawer-paper`]: {
                        width: drawerOpen ? drawerLong : drawerShort,
                        marginTop: 14.8,
                        boxSizing: "border-box",
                        position: "absolute",
                    },
                }}
                variant="persistent"
                anchor="left"
                open={true}
            >
                <Sidebar drawerOpen={drawerOpen} />
            </Drawer>
        </Fragment>
    );
};

export default Navbar;
