import { Fragment, useState } from 'react'
import "../../styles/templates.scss";
import { Grid, List } from "@mui/material";
// import search from '../../../public/assets/images/search.png'
import ROUTES from 'constants/routes';
import { styled } from '@mui/material/styles';
import Paper from '@mui/material/Paper';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Typography from '@mui/material/Typography';
import { CardActionArea } from '@mui/material';
import Button from '@mui/material/Button';
import Navbar from '../../components/Navbar'

const Item = styled(Paper)(({ theme }) => ({
  backgroundColor: theme.palette.mode === 'dark' ? '#1A2027' : '#fff',
  ...theme.typography.body2,
  padding: theme.spacing(1),
  textAlign: 'center',
  color: theme.palette.text.secondary,
}));

const TEMPLATES_ROUTE = {
  route: ROUTES.TEMPLATES,
  title: "Templates",
  icon: <img src="assets/images/Templates.png" alt="notification" className="sidebar__icon" />,
};

interface Props {
  drawerOpen: boolean;
  setDrawerOpen: React.Dispatch<React.SetStateAction<boolean>>;
}
const Templates: React.FC<Props> = ({ drawerOpen, setDrawerOpen }) => {
  const [searched, setsearch] = useState("Search for Templates");
  return (
    <Fragment>
      <Navbar drawerOpen={drawerOpen} setDrawerOpen={setDrawerOpen} />
      <div id="second_nav">
        <span className="nav_two_item">All Templates </span>
        
      </div>
      <Grid id="content_holder">
        <Grid container spacing={2}>
          <Grid item xs={4}>
            <span className='left_side_head'>Use Ready Templates</span>
          </Grid>
          
        </Grid>
        <Grid sx={{ display: 'grid', gridTemplateColumns: 'repeat(4, 1fr)' }}>
          <Card sx={{ maxWidth: 254, maxHeight: 249, marginTop: 5 }}>
            <CardActionArea>
              <CardMedia
                component="img"
                height="80"
                image="assets/images/Bot(2).png"
                alt="green iguana"
                className='card_img'
              />
              <CardContent>
                <Typography gutterBottom variant="h5" component="div" className="card_heading">
                Customer Service Bot 
                </Typography>
                <Typography variant="body2" color="text.secondary">
                Automate repetitive sales and support queries and give your customers instant, and consistent answers 24/7/365.**With Customer Service bot you’ll be able to...
                <div>
                <Button variant="outlined" className="btn">
                <p id="r">Import</p>
                </Button>
                </div>
                </Typography>
              </CardContent>
            </CardActionArea>
          </Card>
          <Card sx={{ maxWidth: 254, maxHeight: 249, marginTop: 5 }}>
            <CardActionArea>
              <CardMedia
                component="img"
                height="140"
                image="assets/images/Bot(2).png"
                alt="green iguana"
                className='card_img'
              />
              <CardContent>
                <Typography gutterBottom variant="h5" component="div" className="card_heading">
                Customer Service Bot
                </Typography>
                <Typography variant="body2" color="text.secondary">
                Automate repetitive sales and support queries and give your customers instant, and consistent answers 24/7/365.**With Customer Service bot you’ll be able to...
                <div>
                <Button variant="outlined" className="btn">
                <p id="r">Import</p>
                </Button>
                </div>
                </Typography>
              </CardContent>
            </CardActionArea>
          </Card>
          <Card sx={{ maxWidth: 254, maxHeight: 249, marginTop: 5 }}>
            <CardActionArea>
              <CardMedia
                component="img"
                height="140"
                image="assets/images/Bot(2).png"
                alt="green iguana"
                className='card_img'
              />
              <CardContent>
                <Typography gutterBottom variant="h5" component="div" className="card_heading">
                Customer Service Bot
                </Typography>
                <Typography variant="body2" color="text.secondary">
                Automate repetitive sales and support queries and give your customers instant, and consistent answers 24/7/365.**With Customer Service bot you’ll be able to...
                <div>
                <Button variant="outlined" className="btn">
                <p id="r">Import</p>
                </Button>
                </div>
                </Typography>
              </CardContent>
            </CardActionArea>
          </Card>
          <Card sx={{ maxWidth: 254, maxHeight: 249, marginTop: 5 }}>
            <CardActionArea>
              <CardMedia
                component="img"
                height="140"
                image="assets/images/Bot(2).png"
                alt="green iguana"
                className='card_img'
              />
              <CardContent>
                <Typography gutterBottom variant="h5" component="div" className="card_heading">
                Customer Service Bot
                </Typography>
                <Typography variant="body2" color="text.secondary">
                Automate repetitive sales and support queries and give your customers instant, and consistent answers 24/7/365.**With Customer Service bot you’ll be able to...
                <div>
                <Button variant="outlined" className="btn">
                <p id="r">Import</p>
                </Button>
                </div>
                </Typography>
              </CardContent>
            </CardActionArea>
          </Card>
        </Grid>
        <Grid sx={{ display: 'grid', gridTemplateColumns: 'repeat(4, 1fr)' }}>
          <Card sx={{ maxWidth: 254, maxHeight: 249, marginTop: 5 }}>
            <CardActionArea>
              <CardMedia
                component="img"
                height="140"
                image="assets/images/Bot(2).png"
                alt="green iguana"
                className='card_img'
              />
              <CardContent>
                <Typography gutterBottom variant="h5" component="div" className="card_heading">
                Customer Service Bot
                </Typography>
                <Typography variant="body2" color="text.secondary">
                Automate repetitive sales and support queries and give your customers instant, and consistent answers 24/7/365.**With Customer Service bot you’ll be able to...
                <div>
                <Button variant="outlined" className="btn">
                <p id="r">Import</p>
                </Button>
                </div>
                </Typography>
              </CardContent>
            </CardActionArea>
          </Card>
          <Card sx={{ maxWidth: 254, maxHeight: 249, marginTop: 5 }}>
            <CardActionArea>
              <CardMedia
                component="img"
                height="140"
                image="assets/images/Bot(2).png"
                alt="green iguana"
                className='card_img'
              />
              <CardContent>
                <Typography gutterBottom variant="h5" component="div" className="card_heading">
                Customer Service Bot
                </Typography>
                <Typography variant="body2" color="text.secondary">
                Automate repetitive sales and support queries and give your customers instant, and consistent answers 24/7/365.**With Customer Service bot you’ll be able to...
                <div>
                <Button variant="outlined" className="btn">
                <p id="r">Import</p>
                </Button>
                </div>
                </Typography>
              </CardContent>
            </CardActionArea>
          </Card>
          <Card sx={{ maxWidth: 254, maxHeight: 249, marginTop: 5 }}>
            <CardActionArea>
              <CardMedia
                component="img"
                height="140"
                image="assets/images/Bot(2).png"
                alt="green iguana"
                className='card_img'
              />
              <CardContent>
                <Typography gutterBottom variant="h5" component="div" className="card_heading">
                Customer Service Bot
                </Typography>
                <Typography variant="body2" color="text.secondary">
                Automate repetitive sales and support queries and give your customers instant, and consistent answers 24/7/365.**With Customer Service bot you’ll be able to...
                <div>
                <Button variant="outlined" className="btn">
                <p id="r">Import</p>
                </Button>
                </div>
                </Typography>
              </CardContent>
            </CardActionArea>
          </Card>
          <Card sx={{ maxWidth: 254, maxHeight: 249, marginTop: 5 }}>
            <CardActionArea>
              <CardMedia
                component="img"
                height="180"
                image="assets/images/Bot(2).png"
                alt="green iguana"
                className='card_img'
              />
              <CardContent>
                <Typography gutterBottom variant="h5" component="div" className="card_heading">
                Customer Service Bot
                </Typography>
                <Typography variant="body2" color="text.secondary">
                Automate repetitive sales and support queries and give your customers instant, and consistent answers 24/7/365.**With Customer Service bot you’ll be able to...
                <div>
                <Button variant="outlined" className="btn">
                <p id="r">Import</p>
                </Button>
                </div>
                </Typography>
              </CardContent>
            </CardActionArea>
          </Card>
          <Card sx={{ maxWidth: 254, maxHeight: 249, marginTop: 5 }}>
            <CardActionArea>
              <CardMedia
                component="img"
                height="180"
                image="assets/images/Bot(2).png"
                alt="green iguana"
                className='card_img'
              />
              <CardContent>
                <Typography gutterBottom variant="h5" component="div" className="card_heading">
                Customer Service Bot
                </Typography>
                <Typography variant="body2" color="text.secondary">
                Automate repetitive sales and support queries and give your customers instant, and consistent answers 24/7/365.**With Customer Service bot you’ll be able to...
                <div>
                <Button variant="outlined" className="btn">
                <p id="r">Import</p>
                </Button>
                </div>
                </Typography>
              </CardContent>
            </CardActionArea>
          </Card>
          <Card sx={{ maxWidth: 254, maxHeight: 249, marginTop: 5 }}>
            <CardActionArea>
              <CardMedia
                component="img"
                height="180"
                image="assets/images/Bot(2).png"
                alt="green iguana"
                className='card_img'
              />
              <CardContent>
                <Typography gutterBottom variant="h5" component="div" className="card_heading">
                Customer Service Bot
                </Typography>
                <Typography variant="body2" color="text.secondary">
                Automate repetitive sales and support queries and give your customers instant, and consistent answers 24/7/365.**With Customer Service bot you’ll be able to...
                <div>
                <Button variant="outlined" className="btn">
                <p id="r">Import</p>
                </Button>
                </div>
                </Typography>
              </CardContent>
            </CardActionArea>
          </Card>
        </Grid>
      </Grid>
    </Fragment>
  )
}

export default Templates
