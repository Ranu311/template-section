import React, { Fragment } from "react";
import Tabs from "@mui/material/Tabs";
import Tab from "@mui/material/Tab";
import { Button, Grid, Box } from "@mui/material";
import "../../styles/botHome.scss";
import { Navbar, Profile, Settings, Billings } from ".";
interface TabPanelProps {
    children?: React.ReactNode;
    index: number;
    value: number;
}

const TabPanel = (props: TabPanelProps) => {
    const { children, value, index, ...other } = props;

    return (
        <div role="tabpanel" hidden={value !== index} id={`simple-tabpanel-${index}`} aria-labelledby={`simple-tab-${index}`} {...other}>
            {value === index && <Fragment>{children}</Fragment>}
        </div>
    );
};

const a11yProps = (index: number) => {
    return {
        id: `simple-tab-${index}`,
        "aria-controls": `simple-tabpanel-${index}`,
    };
};
interface Props {
    drawerOpen: boolean;
    setDrawerOpen: React.Dispatch<React.SetStateAction<boolean>>;
}
const BotHome: React.FC<Props> = ({ drawerOpen, setDrawerOpen }) => {
    const [value, setValue] = React.useState(0);

    const handleChange = (event: React.SyntheticEvent, newValue: number) => {
        setValue(newValue);
    };
    return (
        <Fragment>
            <Navbar drawerOpen={drawerOpen} setDrawerOpen={setDrawerOpen} />
            <Grid container direction="row" justifyContent="space-between" className="tab__container" alignItems="center">
                <Grid item xs={12} container direction="row" className="tab_bar">
                    <Box sx={{ borderBottom: 0, borderColor: "divider" }}>
                        <Tabs
                            value={value}
                            onChange={handleChange}
                            aria-label="basic tabs example"
                            TabIndicatorProps={{
                                style: {
                                    backgroundColor: "#4154E8;",
                                    color: "#4154E8;",
                                },
                            }}
                        >
                            <Tab label="Profile" {...a11yProps(0)} className="tab__label" />
                            <Tab label="Settings" {...a11yProps(1)} className="tab__label" />
                            <Tab label="Billings" {...a11yProps(2)} className="tab__label" />
                        </Tabs>
                    </Box>
                    <Button variant="text" className="tab__button danger">
                        Logout
                    </Button>
                </Grid>
                <TabPanel value={value} index={0}>
                    <Profile drawerOpen={drawerOpen} />
                </TabPanel>
                <TabPanel value={value} index={1}>
                    <Settings />
                </TabPanel>
                <TabPanel value={value} index={2}>
                    <Billings drawerOpen={drawerOpen} />
                </TabPanel>
            </Grid>
        </Fragment>
    );
};

export default BotHome;
