import BotHome from "./BotHome";
import Navbar from "components/Navbar";
import Profile from "./../Profile/Profile";
import Settings from "./../Settings/Settings";
import Billings from "pages/Billings";
export { Navbar, Profile, Settings, Billings };
export default BotHome;
