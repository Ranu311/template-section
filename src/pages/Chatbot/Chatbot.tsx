import React, { Fragment, useState } from "react";
import { Grid, Typography, TextField, Button } from "@mui/material";
import CloseIcon from "@mui/icons-material/Close";
import "styles/chatbot.scss";
import UploadIcon from "@mui/icons-material/Upload";
import ArrowForwardIcon from "@mui/icons-material/ArrowForward";
interface Props {
    showFooter?: boolean;
}
const Chatbot: React.FC<Props> = ({ showFooter }) => {
    const [message, setMessage] = useState("");
    return (
        <Fragment>
            <Grid container direction="row" justifyContent="center" className="chatbot__container">
                <Grid item xs={12} container justifyContent="center" alignItems="center" className="chatbot-header__container">
                    <Typography>
                        Name of Chatbot <br /> <span>Short Name</span>
                    </Typography>
                    <CloseIcon className="closeIcon" />
                </Grid>
                <Grid item xs={12} container direction="row" className="chatbot-message__container">
                    <Grid item xs={12} container justifyContent="flex-start" alignItems="center" className="message__container incoming">
                        <Typography>Hello There, I am your Bot</Typography>
                    </Grid>
                    <br />
                    <Grid item xs={12} container justifyContent="flex-start" alignItems="center" className="message__container incoming">
                        <Typography>Please provide your Name</Typography>
                    </Grid>
                    <br />
                    <Grid item xs={12} container justifyContent="flex-end" alignItems="center" className="message__container outgoing">
                        <Typography>Please provide your Name</Typography>
                    </Grid>
                </Grid>
                <Grid item xs={12} container justifyContent="center" className="suggestions__container" spacing={1}>
                    <Grid item xs="auto" justifyContent="center">
                        <Button variant="contained" className="suggest_btn">
                            Clothing
                        </Button>
                    </Grid>
                    <Grid item xs="auto" justifyContent="center">
                        <Button variant="contained" className="suggest_btn">
                            Accessories
                        </Button>
                    </Grid>
                    <Grid item xs="auto" justifyContent="center">
                        <Button variant="contained" className="suggest_btn">
                            Other
                        </Button>
                    </Grid>
                </Grid>
                <Grid item xs={12} container justifyContent="flex-start" alignItems="center" className="input__container">
                    <Grid item xs={12}>
                        <TextField
                            type="text"
                            variant="standard"
                            fullWidth
                            placeholder="Type Here"
                            value={message}
                            defaultValue=""
                            name="message"
                            onChange={(e) => setMessage(e.target.value)}
                            InputProps={{
                                startAdornment: <UploadIcon className="uploadIcon" />,
                                disableUnderline: true,
                                endAdornment: <ArrowForwardIcon className="sendIcon" />,
                            }}
                        />
                    </Grid>
                </Grid>
                {showFooter && (
                    <Grid item xs={12} container justifyContent="center" alignItems="center" className="footer">
                        <Typography>Powered by Chatfuro</Typography>
                    </Grid>
                )}
            </Grid>
        </Fragment>
    );
};

export default Chatbot;
