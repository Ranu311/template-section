import React, { Fragment, useState } from "react";
import { Button, Grid, Typography, TextField } from "@mui/material";
import "../../styles/profile.scss";
interface Props {
    drawerOpen: boolean;
}
const Profile: React.FC<Props> = ({ drawerOpen }) => {
    const [form, setform] = useState({
        name: "",
        email: "",
        password: "",
        cpassword: "",
        cur_password: "",
        new_email: "",
    });
    const handleChange = (e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
        const { name, value } = e.target;
        setform({
            ...form,
            [name]: value,
        });
    };
    return (
        <Fragment>
            <Grid
                container
                direction="row"
                className={
                    drawerOpen
                        ? "profile__container"
                        : "profile__container close_transition"
                }
            >
                <Grid item xs={6} container justifyContent="flex-start" className="account__container">
                    <Grid item xs={12}>
                        <Typography className="title">Account</Typography>
                    </Grid>
                    <Grid item xs={12} container direction="row" justifyContent="flex-start" alignItems="center" className="input__container">
                        <Grid item xs={3} className="profile-img__contianer">
                            <img src="assets/images/Avtar.png" alt="Profile Pic" className="profile__img" />
                        </Grid>
                        <Grid item xs={9} container justifyContent="flex-start">
                            <Grid item xs={9}>
                                <Typography>Name</Typography>
                            </Grid>
                            <Grid item xs={9}>
                                <TextField
                                    fullWidth
                                    type="text"
                                    placeholder="Name"
                                    variant="outlined"
                                    className="input__field"
                                    name="name"
                                    value={form.name}
                                    onChange={(e) => handleChange(e)}
                                />
                            </Grid>
                        </Grid>
                    </Grid>
                    <Grid item xs={12} container direction="row" justifyContent="flex-start" alignItems="center" className="input__container">
                        <Grid item xs={12}>
                            <Typography>E-mail Id</Typography>
                        </Grid>
                        <Grid item xs={10}>
                            <TextField
                                fullWidth
                                type="email"
                                placeholder="Email"
                                variant="outlined"
                                className="input__field blurred__input"
                                name="email"
                                value={form.email}
                                onChange={(e) => handleChange(e)}
                            />
                        </Grid>
                    </Grid>
                    <Grid item xs={10} container direction="row" justifyContent="flex-start" alignItems="center" className="box">
                        <Grid item xs={12}>
                            <Typography className="title">Change Password</Typography>
                        </Grid>
                        <Grid item xs={12}>
                            <Typography className="input__label">Old Password</Typography>
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                fullWidth
                                type="password"
                                placeholder="Password"
                                variant="outlined"
                                className="input__field"
                                name="password"
                                value={form.password}
                                onChange={(e) => handleChange(e)}
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <Typography className="input__label">New Password</Typography>
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                fullWidth
                                type="password"
                                placeholder="Confirm Password"
                                variant="outlined"
                                className="input__field"
                                value={form.cpassword}
                                onChange={(e) => handleChange(e)}
                                name="cpasword"
                            />
                        </Grid>
                        <Grid item container xs={12} justifyContent="flex-end" alignItems="center" className="btn__container">
                            <Button variant="contained" className="btn">
                                Cancel
                            </Button>
                            <Button variant="contained" className="btn save">
                                Save
                            </Button>
                        </Grid>
                    </Grid>
                </Grid>
                <Grid item xs={6}>
                    <Grid item xs={8} container direction="row" justifyContent="flex-start" alignItems="center" className="box">
                        <Grid item xs={12}>
                            <Typography className="title">Change Email</Typography>
                        </Grid>
                        <Grid item xs={12}>
                            <Typography className="input__label">Current Password</Typography>
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                fullWidth
                                type="password"
                                placeholder="Password"
                                variant="outlined"
                                className="input__field"
                                name="cur_password"
                                value={form.cur_password}
                                onChange={(e) => handleChange(e)}
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <Typography className="input__label">New E-mail</Typography>
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                fullWidth
                                type="email"
                                placeholder="Email"
                                variant="outlined"
                                className="input__field"
                                name="new_email"
                                value={form.new_email}
                                onChange={(e) => handleChange(e)}
                            />
                        </Grid>
                        <Grid item container xs={12} justifyContent="flex-end" alignItems="center" className="btn__container">
                            <Button variant="contained" className="btn">
                                Cancel
                            </Button>
                            <Button variant="contained" className="btn save">
                                Save
                            </Button>
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>
        </Fragment>
    );
};

export default Profile;
