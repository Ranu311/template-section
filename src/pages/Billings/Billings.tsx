import React, { Fragment, useState } from "react";
import "../../styles/billing.scss";
import { Grid, Button, Card, CardContent, Typography } from "@mui/material";
interface Props {
    drawerOpen: boolean;
}
const Billings: React.FC<Props> = ({ drawerOpen }) => {
    const [plan, setPlan] = useState(0);
    return (
        <Fragment>
            <Grid
                container
                direction="row"
                justifyContent="space-between"
                alignItems="flex-start"
                spacing={6}
                className={drawerOpen ? "billing__container" : "billing__container close_transition"}
            >
                {[0, 100, 150].map((index) => {
                    return (
                        <Grid item xs={4}>
                            <Card className={"card card" + (plan !== index ? index : "")} onClick={() => setPlan(index)}>
                                {plan === index && (
                                    <CardContent className="activePlan">
                                        <Typography>Current Plan</Typography>
                                    </CardContent>
                                )}
                                <CardContent>
                                    <Typography gutterBottom variant="h5" component="div" className="card__title">
                                        Basic Package
                                    </Typography>
                                    <Typography variant="body1" className="plan__info">
                                        <ol>
                                            <li>
                                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptatibus non praesentium reprehenderit
                                                error reiciendis repellat porro quos quia minima minus!
                                            </li>
                                            <li>
                                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptatibus non praesentium reprehenderit
                                                error reiciendis repellat porro quos quia minima minus!
                                            </li>
                                        </ol>
                                    </Typography>
                                    <Grid container direction="row" justifyContent="space-between" className="card__footer">
                                        <Typography variant="h5" component="h5">
                                            ${index}.00{index !== 0 ? "/m" : ""}
                                        </Typography>
                                        {plan !== index && <Button variant="contained">Buy</Button>}
                                    </Grid>
                                </CardContent>
                            </Card>
                        </Grid>
                    );
                })}
            </Grid>
        </Fragment>
    );
};

export default Billings;
