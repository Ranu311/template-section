import React, { Fragment, useState } from "react";
import { Button, Grid, Typography, TextField } from "@mui/material";
import "styles/createBot.scss";
import { Chatbot } from ".";
import Checkbox from "@mui/material/Checkbox";
import FormGroup from "@mui/material/FormGroup";
import FormControlLabel from "@mui/material/FormControlLabel";
interface Props {
    drawerOpen: boolean;
}
const BasicInformation: React.FC<Props> = ({ drawerOpen }) => {
    const [form, setform] = useState({
        name: "",
        description: "",
    });
    const [showFooter, setShowFooter] = useState(true);
    const handleChange = (e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
        const { name, value } = e.target;
        setform({
            ...form,
            [name]: value,
        });
    };
    return (
        <Fragment>
            <Grid
                container
                direction="row"
                className={drawerOpen ? "basic-info__container" : "basic-info__container close_transition"}
                justifyContent="space-between"
            >
                <Grid item xs={7} container justifyContent="flex-start" className="info__container">
                    <Grid item xs={8}>
                        <Typography className="title">Basic Information</Typography>
                        <Typography className="subtitle">Tell us the Name and description of the bot</Typography>
                        <Grid item xs={12} container direction="row" justifyContent="flex-start" alignItems="center" className="input__container">
                            <Grid item xs={12}>
                                <Typography>Bot Name</Typography>
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    fullWidth
                                    type="text"
                                    placeholder="Name"
                                    variant="outlined"
                                    className="input__field"
                                    name="name"
                                    value={form.name}
                                    onChange={(e) => handleChange(e)}
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <Typography>Bot Description</Typography>
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    fullWidth
                                    type="text"
                                    placeholder="Description"
                                    variant="outlined"
                                    className="input__field"
                                    name="description"
                                    value={form.description}
                                    onChange={(e) => handleChange(e)}
                                />
                            </Grid>
                            <Grid item container xs={12} justifyContent="flex-end" alignItems="center" className="btn__container">
                                <Button variant="contained" className="btn">
                                    Cancel
                                </Button>
                                <Button variant="contained" className="btn save">
                                    Save
                                </Button>
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>
                <Grid item xs={4} className="chatbot-img__contianer">
                    <Chatbot showFooter={showFooter} />
                </Grid>
                <Grid item xs={12} justifyContent="flex-end">
                    <FormGroup row className="footer_btn">
                        <FormControlLabel
                            value={showFooter}
                            onChange={() => setShowFooter(!showFooter)}
                            control={<Checkbox size="small" />}
                            label="Remove Footer"
                            labelPlacement="start"
                        />
                    </FormGroup>
                </Grid>
            </Grid>
        </Fragment>
    );
};

export default BasicInformation;
