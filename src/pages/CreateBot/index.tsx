import CreateBot from "./CreateBot";
import Navbar from "components/Navbar";
import BasicInformation from "./BasicInformation";
import Chatbot from "pages/Chatbot";
import Flows from "./Flows";
import Personalization from "./Personalization";
export default CreateBot;

export { Navbar, Personalization, Flows, Chatbot, BasicInformation };
