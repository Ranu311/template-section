import React, { Fragment, useState } from "react";
import { Button, Grid, Typography, Box, TextField } from "@mui/material";
import CloudUploadOutlinedIcon from "@mui/icons-material/CloudUploadOutlined";
import "styles/createBot.scss";
import { Chatbot } from ".";
import Checkbox from "@mui/material/Checkbox";
import FormGroup from "@mui/material/FormGroup";
import FormControlLabel from "@mui/material/FormControlLabel";
interface Props {
    drawerOpen: boolean;
}
const Personalization: React.FC<Props> = ({ drawerOpen }) => {
    const [form, setForm] = useState({
        color: "#883333",
    });
    const [showFooter, setShowFooter] = useState(true);

    const handleChange = (e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
        const { name, value } = e.target;
        setForm({
            ...form,
            [name]: value,
        });
    };
    return (
        <Fragment>
            <Grid
                container
                direction="row"
                className={drawerOpen ? "personalization__container" : "personalization__container close_transition"}
                justifyContent="space-between"
            >
                <Grid item xs={7} container justifyContent="flex-start" className="info__container">
                    <Grid item xs={8}>
                        <Typography className="title">Your Chatbot Name</Typography>
                        <Typography className="subtitle">Customize the chatbot with an avatar and create a scheme</Typography>
                        <Grid item xs={12} container direction="row" justifyContent="flex-start" alignItems="center" className="input__container">
                            <Grid item xs={12}>
                                <Typography>Select your color of Chatbot</Typography>
                            </Grid>
                            <Grid item xs={12} container spacing={1}>
                                <Grid item xs={2}>
                                    <Box className="color" id="Aquamarine"></Box>
                                </Grid>
                                <Grid item xs={2}>
                                    <Box className="color" id="MediumSlateBlue"></Box>
                                </Grid>
                                <Grid item xs={2}>
                                    <Box className="color" id="Blue"></Box>
                                </Grid>
                                <Grid item xs={2}>
                                    <Box className="color" id="LightPink"></Box>
                                </Grid>
                                <Grid item xs={2}>
                                    <Box className="color" id="YellowGreen"></Box>
                                </Grid>
                                <Grid item xs={2}>
                                    <Box className="color" id="MidnightBlue"></Box>
                                </Grid>
                                <Grid item xs={2}>
                                    <Box className="color" id="IndianRed"></Box>
                                </Grid>
                                <Grid item xs={2}>
                                    <Box className="color" id="Red"></Box>
                                </Grid>
                                <Grid item xs={2}>
                                    <Box className="color" id="Gold"></Box>
                                </Grid>
                                <Grid item xs={2}>
                                    <Box className="color" id="Orange"></Box>
                                </Grid>
                                <Grid item xs={2}>
                                    <Box className="color" id="Pink"></Box>
                                </Grid>
                                <Grid item xs={2}>
                                    <Box className="color" id="Purple"></Box>
                                </Grid>
                            </Grid>
                            <Grid item xs={12} container justifyContent="flex-start" className="color-picker__container" alignItems="center">
                                <Grid item xs={4}>
                                    <Typography>Pick a Color</Typography>
                                </Grid>
                                <Grid item xs={2}>
                                    <TextField
                                        type="color"
                                        variant="standard"
                                        value={form.color}
                                        name="color"
                                        onChange={(e) => handleChange(e)}
                                        fullWidth
                                        className="color-picker"
                                        InputProps={{
                                            disableUnderline: true,
                                        }}
                                    />
                                </Grid>
                                <Grid item xs={4}>
                                    <Typography>{form.color}</Typography>
                                </Grid>
                            </Grid>
                            <Grid item xs={12}>
                                <Typography>Select avatar for your Chatbot</Typography>
                                <Typography className="sub_title">The file must be 50x50 dimension</Typography>
                            </Grid>
                            <Grid item xs={12} justifyContent="center" alignItems="center" className="avtar-input__container">
                                <Box className="box">
                                    <Grid item xs={12} container justifyContent="center" alignItems="center">
                                        <Grid item xs={12} container justifyContent="center">
                                            <CloudUploadOutlinedIcon className="upload_icon" />
                                        </Grid>
                                        <Grid item xs={12} container justifyContent="center">
                                            <Typography>Drag and Drop File</Typography>
                                        </Grid>
                                        <Grid item xs={12} container justifyContent="center">
                                            <Typography component="span">OR</Typography>
                                        </Grid>
                                        <Grid item xs={12} container justifyContent="center">
                                            <Button variant="contained" className="btn save">
                                                Browse Files
                                            </Button>
                                        </Grid>
                                    </Grid>
                                </Box>
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>
                <Grid item xs={4} className="chatbot-img__contianer">
                    <Chatbot showFooter={showFooter} />
                </Grid>
                <Grid item xs={12} justifyContent="flex-end">
                    <FormGroup row className="footer_btn">
                        <FormControlLabel
                            value={showFooter}
                            onChange={() => setShowFooter(!showFooter)}
                            control={<Checkbox size="small" />}
                            label="Remove Footer"
                            labelPlacement="start"
                        />
                    </FormGroup>
                </Grid>
            </Grid>
        </Fragment>
    );
};

export default Personalization;
