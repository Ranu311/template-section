import React, { Fragment } from "react";
import { Navbar, BasicInformation, Personalization, Flows } from ".";
import Tabs from "@mui/material/Tabs";
import Tab from "@mui/material/Tab";
import { Grid, Box } from "@mui/material";
import "../../styles/botHome.scss";
interface TabPanelProps {
    children?: React.ReactNode;
    index: number;
    value: number;
}

const TabPanel = (props: TabPanelProps) => {
    const { children, value, index, ...other } = props;

    return (
        <div role="tabpanel" hidden={value !== index} id={`simple-tabpanel-${index}`} aria-labelledby={`simple-tab-${index}`} {...other}>
            {value === index && <Fragment>{children}</Fragment>}
        </div>
    );
};

const a11yProps = (index: number) => {
    return {
        id: `simple-tab-${index}`,
        "aria-controls": `simple-tabpanel-${index}`,
    };
};
interface Props {
    drawerOpen: boolean;
    setDrawerOpen: React.Dispatch<React.SetStateAction<boolean>>;
}
const CreateBot: React.FC<Props> = ({ drawerOpen, setDrawerOpen }) => {
    const [value, setValue] = React.useState(0);

    const handleChange = (event: React.SyntheticEvent, newValue: number) => {
        setValue(newValue);
    };
    return (
        <Fragment>
            <Navbar drawerOpen={drawerOpen} setDrawerOpen={setDrawerOpen} />

            <Grid container direction="row" justifyContent="space-between" className="tab__container" alignItems="center">
                <Grid item xs={12} container direction="row" className="tab_bar">
                    <Box sx={{ borderBottom: 0, borderColor: "divider" }}>
                        <Tabs
                            value={value}
                            onChange={handleChange}
                            aria-label="basic tabs example"
                            TabIndicatorProps={{
                                style: {
                                    backgroundColor: "#4154E8;",
                                    color: "#4154E8;",
                                },
                            }}
                        >
                            <Tab label="Basic Information" {...a11yProps(0)} className="tab__label" />
                            <Tab label="Personalization" {...a11yProps(1)} className="tab__label" />
                            <Tab label="Flows" {...a11yProps(2)} className="tab__label" />
                        </Tabs>
                    </Box>
                </Grid>
                <TabPanel value={value} index={0}>
                    <BasicInformation drawerOpen={drawerOpen} />
                </TabPanel>
                <TabPanel value={value} index={1}>
                    <Personalization drawerOpen={drawerOpen} />
                </TabPanel>
                <TabPanel value={value} index={2}>
                    <Flows drawerOpen={drawerOpen} />
                </TabPanel>
            </Grid>
        </Fragment>
    );
};

export default CreateBot;
