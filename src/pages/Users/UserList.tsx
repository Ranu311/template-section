import React, { Fragment } from "react";
import { Grid, Button } from "@mui/material";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";
import KeyboardArrowDownIcon from "@mui/icons-material/KeyboardArrowDown";
import "styles/users.scss";
interface Props {
    drawerOpen: boolean;
}
const UserList: React.FC<Props> = ({ drawerOpen }) => {
    return (
        <Fragment>
            <Grid container direction="row" className={drawerOpen ? "user__container" : "user__container close_transition"}>
                <Grid item container xs={12} justifyContent="flex-end" className="users_actions">
                    <Button variant="text" className="tab__button primary action-btn white">
                        Filters <KeyboardArrowDownIcon />
                    </Button>
                    <Button variant="text" className="tab__button primary action-btn">
                        Add Column <KeyboardArrowDownIcon />
                    </Button>
                </Grid>
                <Grid item xs={12} className="user-list__table">
                    <TableContainer component={Paper} sx={{ minWidth: 650 }}>
                        <Table sx={{ width: "100% " }} aria-label="simple table">
                            <TableHead>
                                <TableRow>
                                    <TableCell align="center" className="table__head">
                                        <img src="assets/images/Person.png" alt="Person" className="table__icon" />
                                        Name
                                    </TableCell>
                                    <TableCell align="center" className="table__head">
                                        <img src="assets/images/Email.png" alt="Email" className="table__icon" />
                                        Email
                                    </TableCell>
                                    <TableCell align="center" className="table__head">
                                        <img src="assets/images/Calender.png" alt="Calender" className="table__icon" />
                                        Last Seen
                                        <img src="assets/images/arrow-up-down.png" alt="Arrow Up Down" className="table__icon up_down" />
                                    </TableCell>
                                    <TableCell align="center" className="table__head">
                                        {" "}
                                        <img src="assets/images/Calender.png" alt="Calender" className="table__icon" />
                                        First Seen
                                        <img src="assets/images/arrow-up-down.png" alt="Arrow Up Down" className="table__icon up_down" />
                                    </TableCell>
                                    <TableCell align="center" className="table__head">
                                        <img src="assets/images/Location.png" alt="location" className="table__icon" />
                                        Country
                                    </TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {[1, 2, 3, 4, 5, 6].map((index) => {
                                    return (
                                        <TableRow key={index}>
                                            <TableCell align="center" className="table__data">
                                                Rajansh muk
                                            </TableCell>
                                            <TableCell align="center" className="table__data">
                                                mathurudit90@gmail.com
                                            </TableCell>
                                            <TableCell align="center" className="table__data">
                                                02-09-2022
                                            </TableCell>
                                            <TableCell align="center" className="table__data">
                                                02-09-2022
                                            </TableCell>
                                            <TableCell align="center" className="table__data">
                                                India
                                            </TableCell>
                                        </TableRow>
                                    );
                                })}
                            </TableBody>
                        </Table>
                    </TableContainer>
                </Grid>
            </Grid>
        </Fragment>
    );
};

export default UserList;
